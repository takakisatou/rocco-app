//
//  FavViewController.swift
//  rocco-app
//
//  Created by mac吉岡 on 2016/12/31.
//  Copyright © 2016年 ROCCO. All rights reserved.
//

import UIKit
import MagicalRecord

class FavViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var entries: [Favorite]!

    override func viewDidLoad() {
        super.viewDidLoad()
        entries = Favorite.mr_findAll() as? [Favorite]


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "FavCell")
        cell.textLabel!.text = entries[indexPath.row].title
        cell.textLabel?.numberOfLines = 3
        cell.detailTextLabel?.text = "追加した日； " + entries[indexPath.row].addDate!
        cell.detailTextLabel?.textColor = UIColor.gray
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
