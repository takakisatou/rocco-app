//
//  ViewController.swift
//  rocco-app
//
//  Created by mac吉岡 on 2016/12/20.
//  Copyright © 2016年 ROCCO. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    // entriesデータを格納する配列
    var entryDataArray = NSArray()
    var imageCache = [String:UIImage]() //thumbnail用のimageCache
    var entryUrl = "" // 記事のURLを格納するString変数
    var entryTitle = "" // 記事のタイトルを格納するString変数

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ROCCO" //ViewControllerのタイトル設定
        table.dataSource = self
        table.delegate = self
        
        let now = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/ddHH:mm"
        let addeddate = formatter.string(from: now as Date)
        
        // Do any additional setup after loading the view, typically from a nib.
//        let requestUrl = "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=https://applewatchjournal.net/feed/&num=20"
        let requestUrl = "http://rocco-girl.com/feed/json/?date=" + addeddate

//        Alamofire.request(.GET, requestUrl).responseJSON{
//        (request, response, json, error) in
//            print(json)
//        }

        Alamofire.request(requestUrl).responseJSON { response in
            print(response.request!)  // original URL request
            print(response.response!) // HTTP URL response
            print(response.data!)     // server data
            print(response.result)   // result of response serialization

            let jsonDic = response.result.value as! NSDictionary
            let responseData = jsonDic["responseData"] as! NSDictionary
            let feed = responseData["feed"] as! NSDictionary
            self.entryDataArray = feed["entries"] as! NSArray
            self.table.reloadData() //取得したデータをテーブルビューに表示

//            print(self.entryDataArray)
        }


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entryDataArray.count
    }
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! CustomCell
        //storyboardで作成した"Cell"を取得
        cell.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        let entryDic = entryDataArray[indexPath.row] as! NSDictionary //配列の"indexPath.row"番目の要を取得

        // 取得した要素からタイトル、掲載日、カテゴリー（複数選択している場合は最初のカテゴリー）をCellにセット
        cell.title.text = entryDic["title"] as? String
        cell.pubDate.text = entryDic["publishedDate"] as? String
        let category = entryDic["categories"] as! NSArray
        cell.category.text = category[0] as? String
        cell.author.text = entryDic["author"] as? String

        // JSONデータの中から、thumbnail画像のURLを取得し、NSURL型に変換（この辺り、力技すぎるのでもう少しかっこいい処理がしたいところですが…。）
//        var mediaGroups:NSArray = (entryDic["mediaGroups"] as? NSArray)!
//        var mediacontents:NSDictionary = (mediaGroups[0] as? NSDictionary)!
//        mediaGroups = (mediacontents["contents"] as? NSArray)!
//        mediacontents = (mediaGroups[0] as? NSDictionary)!
//        let urlString = mediacontents["url"] as! String
//        let imgURL: NSURL? = NSURL(string: urlString)
        let urlString = entryDic["thumbnail"] as? String
        let imgURL : NSURL? = NSURL ( string: urlString!)

        // キャッシュがあればキャッシュを表示、なければ非同期通信で画像データを取得して表示
        if let img = imageCache[urlString!] {
            cell.thumbnail?.image = img
        }else{
            if(imgURL != nil){
                let request: NSURLRequest = NSURLRequest ( url: imgURL! as URL)
                    let configuration = URLSessionConfiguration.default
    let session = URLSession(configuration: configuration, delegate:nil, delegateQueue:OperationQueue.main)
    let task = session.dataTask(with: request as URLRequest, completionHandler: {
        (data, response, error) -> Void in
        do {
            
                        let image = UIImage(data: data!)
                        self.imageCache[urlString!] = image
                        cell.thumbnail?.image = image
//                        DispatchQueue.main.sync (execute: {
//                            cell.thumbnail?.image = image
//                        })
            
        } catch {
            
                        print("Error: \(error.localizedDescription)")
        }
        
    })
    
    task.resume()
//                let request: NSURLRequest = NSURLRequest(url: imgURL! as URL)
//                let mainQueue = OperationQueue.main
//                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//                    if error == nil {
//                        let image = UIImage(data: data!)
//                        self.imageCache[urlString!] = image
//                        DispatchQueue.main.sync (execute: {
//                            cell.thumbnail?.image = image
//                        })
//                    }else {
//                        print("Error: \(error?.localizedDescription)")
//                    }
//                })
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entryDic = entryDataArray[indexPath.row] as! NSDictionary // 記事データの配列からindexPath.row番目の要素を取得
        entryUrl = entryDic["link"] as! String // 記事のURLを取得
        entryTitle = entryDic["title"] as! String // 記事のタイトルを取得
        performSegue(withIdentifier: "toWebView", sender: self) // Identifierを"toWebView"と命名したセグエを実行し、WebViewController画面に遷移

        tableView.deselectRow(at: indexPath, animated: true) //タップした際のハイライトを解除
    }
    override func prepare( for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toWebView") {
            let wvc = segue.destination as! WebViewController // セグエ用にダウンキャストしたWebViewCOntrollerのインスタンス
            wvc.entryUrl = entryUrl + "?utm_source=rocco-app&utm_medium=app&utm_campaign=from-app"
            wvc.entryTitle = entryTitle
            print("endtryUrl"+entryUrl)
        }
    }
}

