//
//  Favorite+CoreDataProperties.swift
//  rocco-app
//
//  Created by mac吉岡 on 2016/12/31.
//  Copyright © 2016年 ROCCO. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Favorite {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favorite> {
        return NSFetchRequest<Favorite>(entityName: "Favorite");
    }

    @NSManaged public var addDate: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?

}
