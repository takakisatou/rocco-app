//
//  WebViewController.swift
//  rocco-app
//
//  Created by mac吉岡 on 2016/12/26.
//  Copyright © 2016年 ROCCO. All rights reserved.
//

import UIKit

class WebViewController: UIViewController ,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    var entryUrl = "" // URLを格納するString変数
    var entryTitle = "" // タイトルを格納するString変数
      var indicator = UIActivityIndicatorView() // インジケータを使うための変数
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self //UIWebViewDelegateの設定
        indicator.center = self.view.center //インジケータを画面中央に表示
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray //インジケータをグレーに設定
        webView.addSubview(indicator) // インジケータをWebView上に配置
        
        let url = NSURL(string: entryUrl)! // String変数をNSURLに変換
        let urlRequest = NSURLRequest(url: url as URL) // NSURLRequestにURL情報を渡す
        webView.loadRequest(urlRequest as URLRequest) // NSURLRequestを使って、UIWebViewクラスのloadメソッドをコール

        self.navigationItem.title = entryTitle // Navigationのタイトルに記事タイトルを設定
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        indicator.startAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        indicator.stopAnimating()

        let css:String = ".site-header,entry-stats ul,.breadcrumb,footer{display:none;} #return{padding-top: 0;} .blogbox{border-top: none;}" //Custom CSS
        var jsscript = "var style = document.createElement('style');style.type = 'text/css';var cssContent = document.createTextNode('"
        jsscript += css
        jsscript += "');style.appendChild(cssContent);document.body.appendChild(style);"

        webView.stringByEvaluatingJavaScript(from: jsscript)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
