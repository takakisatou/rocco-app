//
//  CustomCell.swift
//  rocco-app
//
//  Created by mac吉岡 on 2016/12/23.
//  Copyright © 2016年 ROCCO. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var pubDate: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var author: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
